//
//  NimbleCheckinAppApp.swift
//  NimbleCheckinApp
//
//  Created by Dee Odus on 14/02/2022.
//

import SwiftUI

@main
struct NimbleCheckinAppApp: App {
    var body: some Scene {
        WindowGroup {
			TabBarView()
        }
    }
}
