//
//  Location.swift
//  NimbleCheckinApp
//
//  Created by Dee Odus on 14/02/2022.
//

import Foundation
import CoreLocation

enum LocationName: String{
	
	case manchester, leeds, sheffield, home
}

struct Location{
	
	let name: LocationName
	let coordinate: CLLocationCoordinate2D?
}

struct Profile: Identifiable{
	
	let id = UUID()
	let name: String
	let active: Bool
	let phone: String
	let location: Location
	let photoUrl: String = "https://picsum.photos/300"
}
