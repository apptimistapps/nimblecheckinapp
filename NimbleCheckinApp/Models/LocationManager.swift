//
//  LocationManager.swift
//  NimbleCheckinApp
//
//  Created by Dee Odus on 15/02/2022.
//

import Foundation
import CoreLocation
import SwiftUI

class LocationManager: NSObject, ObservableObject, CLLocationManagerDelegate{
	
	static let shared = LocationManager()
	
	let manager = CLLocationManager()
	
	@Published var location: CLLocationCoordinate2D?
	@Published var isLoading = false
	
	override init() {
		super.init()
		manager.delegate = self
	}
	
	func requestLocation(){
		
		isLoading = true
		manager.requestWhenInUseAuthorization()
		
		manager.requestLocation()
	}
	
	func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
		
		location = locations.first?.coordinate
		isLoading = false
	}
	
	func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
		
		print("error getting locaton", error.localizedDescription)
		isLoading = false
	}
}
