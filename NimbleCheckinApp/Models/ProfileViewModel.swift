//
//  ProfileManager.swift
//  NimbleCheckinApp
//
//  Created by Dee Odus on 14/02/2022.
//

import Foundation
import CoreLocation

final class ProfileViewModel: ObservableObject{
	
	@Published var profile: Profile
	@Published var profiles: [Profile] = []
	
	init(){
		
		self.profile = Profile(name: "Dee Odus", active: true, phone: "0123456789", location: Location(name: .manchester, coordinate: CLLocationCoordinate2D(latitude: 53.487777, longitude: -2.241111)))
		
		self.profiles = [
			
			Profile(name: "Dee Odus", active: true, phone: "0123456789", location: Location(name: .manchester, coordinate: CLLocationCoordinate2D(latitude: 53.585259, longitude: -2.222644))),
			
			Profile(name: "James Brown", active: false, phone: "0123456789", location: Location(name: .leeds, coordinate: CLLocationCoordinate2D(latitude: 53.891277, longitude: -1.598567))),
			
			Profile(name: "Chris Hunt", active: false, phone: "0123456789", location: Location(name: .sheffield, coordinate: CLLocationCoordinate2D(latitude: 53.399931, longitude: -1.499967))),
			
			Profile(name: "Chris Roberts", active: true, phone: "0123456789", location: Location(name: .home, coordinate: nil)),
			
			Profile(name: "Andy Donnelly", active: false, phone: "0123456789", location: Location(name: .manchester, coordinate: CLLocationCoordinate2D(latitude: 53.783129, longitude: -2.244234))),
			
			Profile(name: "Daniella", active: true, phone: "0123456789", location: Location(name: .manchester, coordinate: CLLocationCoordinate2D(latitude: 53.683444, longitude: -2.246664))),
			
			Profile(name: "Tim", active: false, phone: "0123456789", location: Location(name: .sheffield, coordinate: CLLocationCoordinate2D(latitude: 53.388831, longitude: -1.488867))),
			
			Profile(name: "Vic", active: true, phone: "0123456789", location: Location(name: .leeds, coordinate: CLLocationCoordinate2D(latitude: 53.888877, longitude: -1.588887))),
			
			Profile(name: "Penny Arnold", active: false, phone: "0123456789", location: Location(name: .home, coordinate: nil)),
			
			Profile(name: "Anthony Robers", active: false, phone: "0123456789", location: Location(name: .leeds, coordinate: CLLocationCoordinate2D(latitude: 53.871277, longitude: -1.578567))),
			
			Profile(name: "Ria", active: true, phone: "0123456789", location: Location(name: .manchester, coordinate: CLLocationCoordinate2D(latitude: 53.467829, longitude: -2.26664)))
		]
	}
	
	func filterProfiles(by location: LocationName) -> [Profile]{
		
		return profiles.filter{ $0.location.name == location }
	}
	
	func findUserProfile(by searchString: String) -> Profile?{
		
		return profiles.filter{ $0.name.contains(searchString) }.first
	}
}
