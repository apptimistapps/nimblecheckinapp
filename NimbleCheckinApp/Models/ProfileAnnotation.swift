//
//  ProfileAnnotation.swift
//  NimbleCheckinApp
//
//  Created by Dee Odus on 15/02/2022.
//

import Foundation
import MapKit

final class ProfileAnnotation: NSObject, MKAnnotation{
	
	let title: String?
	let coordinate: CLLocationCoordinate2D
	let profile: Profile
	
	init(profile: Profile, coordinate: CLLocationCoordinate2D){
		
		self.title = profile.name
		self.profile = profile
		self.coordinate = coordinate
	}
	
	var subtitle: String? {
		return profile.location.name.rawValue
	}
}
