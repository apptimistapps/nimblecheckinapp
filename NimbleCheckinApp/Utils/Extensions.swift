//
//  Extensions.swift
//  NimbleCheckinApp
//
//  Created by Dee Odus on 16/02/2022.
//

import UIKit
extension UIImage{
	
	static func getImage(from urlString: String) -> UIImage?{
		
		guard let url = URL(string: urlString) else { return nil }
		guard let data = try? Data(contentsOf: url) else { return nil }
		
		return UIImage(data: data)
		
	}
}
