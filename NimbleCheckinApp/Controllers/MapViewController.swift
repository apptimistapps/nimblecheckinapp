//
//  MapViewController.swift
//  NimbleCheckinApp
//
//  Created by Dee Odus on 15/02/2022.
//


import UIKit
import MapKit
import SwiftUI

final class MapViewController: UIViewController {

	private let regionRadius: Double = 20000
	@IBOutlet weak private var mapView: MKMapView!
	
	//MARK: Lifecycle
	override func viewDidLoad() {
		super.viewDidLoad()

		mapView.delegate = self
	}
	
	override func viewDidAppear(_ animated: Bool) {
		super.viewDidAppear(animated)
		
		let profiles = ProfileViewModel().profiles
		addAnnotationToMap(for: profiles)
	}
	
	
	//MARK: Helper Functions
	func centerMapOnUserLocation(location: CLLocationCoordinate2D) {

		let coordinateRegion = MKCoordinateRegion(center: location, latitudinalMeters: regionRadius, longitudinalMeters: regionRadius)
		mapView.setRegion(coordinateRegion, animated: true)
	}
	
	func addAnnotationToMap(for profiles: [Profile]){

		for profile in profiles{
			
			guard let location = profile.location.coordinate else { continue }
			
			let profileAnnonation = ProfileAnnotation(profile: profile, coordinate: location)
			mapView.addAnnotation(profileAnnonation)
		}

		guard let firstProfile = profiles.first else { return }
		guard let coordinate = firstProfile.location.coordinate else { return }
		centerMapOnUserLocation(location: coordinate)
	}
	
	func presentDetailsView(profile: Profile){
		
		let vc = UIHostingController(rootView: DetailsView(profile: profile))
		present(vc, animated: true)
	}
}

//MARK: MKMapViewDelegate functions
extension MapViewController: MKMapViewDelegate{
	
	func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
		
		guard let annotation = annotation as? ProfileAnnotation else {
			return nil
		}
		
		let identifier = "offices"
		let annotationView = MKPinAnnotationView(annotation:annotation, reuseIdentifier:identifier)
		annotationView.isEnabled = true
		annotationView.canShowCallout = true
		
		annotationView.image = UIImage.init(named: "nimble")
		annotationView.frame.size = CGSize(width: 25, height: 40)

		let btn = UIButton(type: .detailDisclosure)
		annotationView.rightCalloutAccessoryView = btn
		return annotationView
	}
	
	func mapView(_ mapView: MKMapView, annotationView view: MKAnnotationView, calloutAccessoryControlTapped control: UIControl) {
		
		guard let profileAnnotation = view.annotation as? ProfileAnnotation else { return }
		print(profileAnnotation.profile.name, profileAnnotation.profile.phone)
		
		presentDetailsView(profile: profileAnnotation.profile)
	}
}
