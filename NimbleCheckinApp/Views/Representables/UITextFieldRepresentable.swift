//
//  UITextFieldRepresentable.swift
//  NimbleCheckinApp
//
//  Created by Dee Odus on 17/02/2022.
//

import Foundation
import SwiftUI

struct UITextFieldViewRepresentable: UIViewRepresentable{
	
	@Binding var text: String
	
	func makeUIView(context: Context) -> some UIView {
		
		let textfield = UITextField(frame: .zero)
		textfield.placeholder = "Search for a user"
		textfield.delegate = context.coordinator
		
		return textfield
	}
	
	func updateUIView(_ uiView: UIViewType, context: Context) {
		
	}
	
	func makeCoordinator() -> Coordinator {
		
		return Coordinator(text: $text)
	}
	
	class Coordinator: NSObject, UITextFieldDelegate{
		
		@Binding var text: String
		
		init(text: Binding<String>) {
			self._text = text
		}
		
		func textFieldDidChangeSelection(_ textField: UITextField) {
			
			text = textField.text ?? ""
		}
		
		func textFieldShouldReturn(_ textField: UITextField) -> Bool {
			textField.resignFirstResponder()
			
			return false
		}
	}
}
