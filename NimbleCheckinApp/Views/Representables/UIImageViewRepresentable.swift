//
//  UIImageViewRepresentation.swift
//  NimbleCheckinApp
//
//  Created by Dee Odus on 17/02/2022.
//

import Foundation
import SwiftUI
struct UIImageViewRepresentable: UIViewRepresentable{
	
	var photoURL: String
	
	func makeUIView(context: Context) -> UIImageView {
		
		let imageView = RoundedImageView(frame: .zero)
		let image = UIImage.getImage(from: photoURL)
		
		imageView.image = image
		imageView.contentMode = .scaleAspectFit
		
		// This will allow you to set .frame(width:height:) to any size you want.
		//https://stackoverflow.com/questions/57969470/trouble-to-make-a-custom-uiview-aspect-scale-fit-fill-with-swiftui
		imageView.setContentCompressionResistancePriority(.defaultLow, for: .horizontal)
		imageView.setContentCompressionResistancePriority(.defaultLow, for: .vertical)
		
		return imageView
	}
	
	func updateUIView(_ uiView: UIViewType, context: Context) {
		
		
	}
}
