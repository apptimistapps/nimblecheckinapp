//
//  MapView.swift
//  NimbleCheckinApp
//
//  Created by Dee Odus on 15/02/2022.
//

import MapKit
import SwiftUI

struct MapViewControllerRepresentable: UIViewControllerRepresentable{
	
	@Binding var searchString: String
	@EnvironmentObject var profileManager: ProfileViewModel
	
	func makeUIViewController(context: Context) -> MapViewController {
		
		let storyboard = UIStoryboard(name: "Main", bundle: nil)
		let vc = storyboard.instantiateViewController(withIdentifier: "MapViewController") as? MapViewController
		return vc ?? MapViewController()
	}
	
	func updateUIViewController(_ uiViewController: UIViewControllerType, context: Context) {
		
		let userProfile = profileManager.findUserProfile(by: searchString)
		guard let coordinate = userProfile?.location.coordinate else { return }
		uiViewController.centerMapOnUserLocation(location: coordinate)
	}
	
}
