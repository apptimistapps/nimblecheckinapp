//
//  ProfileView.swift
//  NimbleCheckinApp
//
//  Created by Dee Odus on 14/02/2022.
//

import SwiftUI

struct ProfileView: View {
	
	@State private var name: String = ""
	@State private var phone: String = ""
	@State private var checked: Bool = false
	
    var body: some View {
        
		NavigationView{
			
			
			VStack{
				
				VStack(alignment: .leading){
					
					Text("Name")
					TextField("Enter your name", text: $name)
						.padding()
						.background(.white)
						.cornerRadius(10)
					
					Text("Phone Number")
					TextField("Enter your phone", text: $phone)
						.padding()
						.background(.white)
						.cornerRadius(10)
					
				}
				
				Button {
					
				} label: {
					
					Text("Save Profile")
						
						.frame(width: 200, height: 50, alignment: .center)
						.foregroundColor(.white)
						.background(.blue)
						.cornerRadius(10)
						.padding()
					
				}
				
				Spacer()
				
				HStack(alignment: .center){
					
					Text("Active Status")
					
					Toggle(isOn: $checked) {
						
					}
				}
				.padding([.all], 30)
				
				Spacer()
			}
			.padding([.top, .leading, .trailing], 40)
			.background(Color.init(red: 232/255, green: 232/255, blue: 232/255))
			.navigationTitle("Profile")
			
		}
    }
}

struct ProfileView_Previews: PreviewProvider {
    static var previews: some View {
		ProfileView()
    }
}
