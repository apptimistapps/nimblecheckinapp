//
//  MapView.swift
//  NimbleCheckinApp
//
//  Created by Dee Odus on 15/02/2022.
//

import SwiftUI
import CoreLocationUI

struct MapView: View {
	
	@State private var text:String = ""
	@EnvironmentObject var profileManager: ProfileViewModel
	
    var body: some View {
        
		VStack{
			
			//to be refactored into a view model
			let profile = profileManager.findUserProfile(by: text)
			let details = "\(profile?.name.capitalized ?? "" ) is in \(profile?.location.name.rawValue.capitalized ?? ""). Status is \(profile?.active ?? false == true ? "active and can be reached on \(profile?.phone ?? "")" : "inactive")"
			
			Text(text.isEmpty ? ""  : details)
				.padding([.top, .leading, .trailing], 20)
				.font(.system(size: 20, weight: .bold))
			
			UITextFieldViewRepresentable(text: $text)
				.frame(height: 50)
				.padding(.horizontal)
				.background(Color.init(red: 0.9, green: 0.9, blue: 0.9))
				
			
			MapViewControllerRepresentable(searchString: $text)

		}
    }
}

struct MapView_Previews: PreviewProvider {
    static var previews: some View {
        MapView()
    }
}
