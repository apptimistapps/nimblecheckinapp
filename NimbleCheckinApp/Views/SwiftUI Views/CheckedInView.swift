//
//  CheckedInView.swift
//  NimbleCheckinApp
//
//  Created by Dee Odus on 14/02/2022.
//

import SwiftUI

struct CheckedInView: View {
	
	let location: LocationName
	@EnvironmentObject var profileManager: ProfileViewModel
	
    var body: some View {
		
		NavigationView{
			
			List{
				
				SectionView(profiles: profileManager.filterProfiles(by: .manchester))
				
				SectionView(profiles: profileManager.filterProfiles(by: .leeds))
				
				SectionView(profiles: profileManager.filterProfiles(by: .sheffield))
				
				SectionView(profiles: profileManager.filterProfiles(by: .home))
				
			}
			.navigationTitle("Active Users")
			.toolbar{
				Button("Check-in") {
					
					
				}
			}
			.listStyle(.insetGrouped)
		}
    }
	
}

struct CheckedInView_Previews: PreviewProvider {
    static var previews: some View {
		CheckedInView(location: .leeds)
    }
}
