//
//  AlertView.swift
//  NimbleCheckinApp
//
//  Created by Dee Odus on 15/02/2022.
//

import SwiftUI

struct DetailsView: View {
	
	var profile: Profile?
	
    var body: some View {
		
		VStack(spacing: 20){
			
			Spacer()
			
			UIImageViewRepresentable(photoURL: profile?.photoUrl ?? "")
				.frame(width: 200, height: 200)
				.fixedSize()
			
			Text(profile?.name ?? "")
				.font(.system(size: 40)).bold()
			
			Text("\(profile?.location.name.rawValue.capitalized ?? "Unknown") office")
				.foregroundColor(.gray)
			
			Text(profile?.phone ?? "")
				.padding()
			Spacer()
		}
	
    }
}

struct DetailsView_Previews: PreviewProvider {
    static var previews: some View {
		DetailsView()
    }
}
