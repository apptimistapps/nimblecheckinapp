//
//  SectionView.swift
//  NimbleCheckinApp
//
//  Created by Dee Odus on 14/02/2022.
//

import SwiftUI

struct SectionView: View {
	
	let profiles: [Profile]
	
    var body: some View {
		
		Section(header: Text(profiles.first?.location.name.rawValue ?? "")){
			
			ForEach(profiles) { profile in
				
				CheckinCellView(name: profile.name, status: profile.active, photoURL: profile.photoUrl)

			}
		}
    }
}

struct SectionView_Previews: PreviewProvider {
    static var previews: some View {
		SectionView(profiles: [])
    }
}
