//
//  ContentView.swift
//  NimbleCheckinApp
//
//  Created by Dee Odus on 14/02/2022.
//

import SwiftUI

struct TabBarView: View {
	
	var profileManager = ProfileViewModel()
	
    var body: some View {
        
		TabView{
			
			CheckedInView(location: .manchester)
				.tabItem{
					Label("Nimble Check-ins", systemImage: "location.fill")
				}
			
			MapView()
				.tabItem{
					Label("Map", systemImage: "map.fill")
				}
			
			ProfileView()
				.tabItem{
					Label("My Profile", systemImage: "person.fill")
				}
		}
		.environmentObject(profileManager)
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
		TabBarView()
    }
}
