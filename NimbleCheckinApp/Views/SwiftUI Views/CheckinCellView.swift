//
//  CheckinCellView.swift
//  NimbleCheckinApp
//
//  Created by Dee Odus on 14/02/2022.
//

import SwiftUI

struct CheckinCellView: View {
	
	var name: String
	var status: Bool
	var photoURL: String
	
    var body: some View {
        
		HStack{
			UIImageViewRepresentable(photoURL: photoURL)
				. frame(width: 25, height: 25)
			Text(name)
			Spacer()
			Circle()
				.frame(width: 10, height: 10)
				.foregroundColor(status ? .green : .gray)
		}
		.padding(.horizontal)
    }
}

struct CheckinCellView_Previews: PreviewProvider {
    static var previews: some View {
		CheckinCellView(name: "", status: false, photoURL: "")
    }
}
